/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.co.nichiwa_system.satellite.imaging.prototype;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author uokumura
 */
public interface ImageSink {

    void update(Collection<? extends BufferedImage> images);
    
    public class ImageSourceSupport {
        private final List<ImageSink> list = new ArrayList<>();
        
        void add(ImageSink... sinks) {
            list.addAll(Arrays.asList(sinks));
        }

        public void update(Collection<? extends BufferedImage> images) {
            list.stream().forEach(sink->sink.update(images));
        }
    }
}
