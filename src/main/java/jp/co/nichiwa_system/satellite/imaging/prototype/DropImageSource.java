/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.co.nichiwa_system.satellite.imaging.prototype;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTargetAdapter;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import javax.imageio.ImageIO;
import jp.co.nichiwa_system.satellite.imaging.prototype.ImageSink.ImageSourceSupport;

/**
 *
 * @author uokumura
 */
class DropImageSource extends DropTargetAdapter implements ImageSource {
    
    private static final DataFlavor FILE_LIST_FLAVOR = DataFlavor.javaFileListFlavor;
    
    private final ImageSourceSupport sinks = new ImageSourceSupport();

    @Override
    public void addSink(ImageSink... adds) {
        sinks.add(adds);
    }

    @Override
    public void dragOver(DropTargetDragEvent event) {
        if (event.isDataFlavorSupported(DataFlavor.javaFileListFlavor)) {
            event.acceptDrag(DnDConstants.ACTION_COPY);
        }
    }

    @Override
    public void drop(DropTargetDropEvent event) {
        if (!event.isDataFlavorSupported(FILE_LIST_FLAVOR)) {
            event.rejectDrop();
            return;
        }
        event.acceptDrop(DnDConstants.ACTION_COPY);

        sinks.update(dropedFiles(event).stream().map(DropImageSource::readImage)
                .filter(it -> it != null).collect(Collectors.toList()));

        event.dropComplete(true);
    }    

    private List<File> dropedFiles(DropTargetDropEvent event) {
        try {
            return (List<File>) event.getTransferable().getTransferData(FILE_LIST_FLAVOR);
        } catch (UnsupportedFlavorException | IOException ex) {
            return Collections.emptyList();
        }
    }

    private static BufferedImage readImage(File file) {
        try {
            return ImageIO.read(file);
        } catch (IOException e) {
            return null;
        }
    }
}
