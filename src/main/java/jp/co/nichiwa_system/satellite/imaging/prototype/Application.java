/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.co.nichiwa_system.satellite.imaging.prototype;

import java.awt.dnd.DropTarget;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import org.bytedeco.javacpp.opencv_core;
import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacpp.opencv_imgproc;
import static org.bytedeco.javacpp.opencv_imgproc.CV_BGR2GRAY;
import static org.bytedeco.javacpp.opencv_imgproc.THRESH_OTSU;
import static org.bytedeco.javacpp.opencv_imgproc.erode;

/**
 *
 * @author uokumura
 */
public class Application {
    public static void main(String[] args) {
        
        DropImageSource drop = new DropImageSource();
        List<ImageSource> sources = compressChain(drop);

        new JFrame("地球検出プロトタイプ") {
            {
                setDropTarget(new DropTarget(this, drop));
                
                Box box = new Box(BoxLayout.X_AXIS);
                sources.forEach(source->box.add(new ImageViewer(source)));
                add(new JScrollPane(box));
                
                setSize(800, 600);
                setDefaultCloseOperation(DISPOSE_ON_CLOSE);
                setVisible(true);
            }
        };
    }

    private static List<ImageSource> findEllipseChain(DropImageSource drop) {
        return filterChain(drop,
                ImageFunction.fromOpenCV((src, dst)->opencv_imgproc.cvtColor(src, dst, CV_BGR2GRAY)),
                ImageFunction.fromOpenCV((src, dst)->opencv_imgproc.medianBlur(src, dst, 7)),
                ImageFunction.fromOpenCV((src, dst)->opencv_imgproc.threshold(src, dst, 0, 255, THRESH_OTSU)),
                ImageFunction.fromOpenCV((src, dst)->erode(src, dst, new Mat(), null, 5, opencv_core.BORDER_CONSTANT, null)),
                ImageFunction::findEllipse
        );
    }

    private static List<ImageSource> compressChain(DropImageSource drop) {
        return filterChain(drop,
                ImageFunction.scale(640, 480),
                ImageFunction::bayer,
                ImageFunction.shrink(40)
        );
    }

    private static List<ImageSource> filterChain(final ImageSource source, final Function<BufferedImage, BufferedImage>... conversions) {
        List<ImageSource> sources = new ArrayList<>(/*Arrays.asList(source)*/);
        Arrays.stream(conversions).forEach((conversion) -> {
            final ImageFilter imageFilter = new ImageFilter(conversion);
            final ImageSource prev = sources.isEmpty() ? source : sources.get(sources.size() - 1);
            prev.addSink(imageFilter);
            sources.add(imageFilter);
        });
        return sources;
    }
}
