/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.co.nichiwa_system.satellite.imaging.prototype;

import java.awt.image.BufferedImage;
import java.util.Collection;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 *
 * @author uokumura
 */
public class ImageFilter implements ImageSource, ImageSink {
    
    private final ImageSourceSupport sinks = new ImageSourceSupport();
    
    private final Function<BufferedImage, BufferedImage> conversion;
    
    public ImageFilter(Function<BufferedImage, BufferedImage> conversion) {
        this.conversion = conversion;
    }
    
    @Override
    public void addSink(ImageSink... adds) {
        sinks.add(adds);
    }

    @Override
    public void update(Collection<? extends BufferedImage> images) {
        sinks.update(images.stream().map(conversion).collect(Collectors.toList()));
    }
}
