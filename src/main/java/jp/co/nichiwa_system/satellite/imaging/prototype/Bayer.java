/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.co.nichiwa_system.satellite.imaging.prototype;

/**
 *
 * @author uokumura
 */
public class Bayer {
    public static Filter at(int x, int y) {
        if (x%2 == 0 && y%2 == 0) {
            return Filter.BLUE;
        } else if (x%2 == 1 && y%2 == 1) {
            return Filter.RED;
        } else {
            return Filter.GREEN;
        }
    }

    @FunctionalInterface            
    public static interface Filter {
        static final Filter
                RED   = c -> c & 0xFF0000,
                GREEN = c -> c & 0xFF00,
                BLUE  = c -> c & 0xFF;
        int filter(int color);
    }
}
