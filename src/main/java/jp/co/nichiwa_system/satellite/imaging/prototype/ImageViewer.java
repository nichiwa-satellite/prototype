/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.co.nichiwa_system.satellite.imaging.prototype;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 *
 * @author uokumura
 */
class ImageViewer extends Box {
    public ImageViewer(ImageSource source) {
        super(BoxLayout.Y_AXIS);
        source.addSink(images -> {
            removeAll();
            images.stream().forEach(image -> add(new JLabel(new ImageIcon(image))));
            revalidate();
        });
    }
}
