/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.co.nichiwa_system.satellite.imaging.prototype;

import java.awt.image.BufferedImage;
import java.util.function.Function;
import java.util.function.BiConsumer;
import org.bytedeco.javacpp.opencv_core;
import org.bytedeco.javacpp.opencv_imgproc;
import static org.bytedeco.javacpp.opencv_imgproc.*;
import org.bytedeco.javacv.Java2DFrameConverter;
import org.bytedeco.javacv.OpenCVFrameConverter;

/**
 *
 * @author uokumura
 */
public class ImageFunction {

    public static Function<BufferedImage, BufferedImage> fromOpenCV(BiConsumer<opencv_core.Mat, opencv_core.Mat> conversion) {
        final OpenCVFrameConverter.ToMat toMat = new OpenCVFrameConverter.ToMat();
        return (BufferedImage img) -> {
            opencv_core.Mat src = toMat.convert(new Java2DFrameConverter().convert(img));
            opencv_core.Mat dst = new opencv_core.Mat(src.size(), src.type());
            conversion.accept(src, dst);
            return new Java2DFrameConverter().convert(toMat.convert(dst));
        };
    }

    public static BufferedImage findEllipse(BufferedImage img) {
        final OpenCVFrameConverter.ToMat toMat = new OpenCVFrameConverter.ToMat();
        opencv_core.Mat src = toMat.convert(new Java2DFrameConverter().convert(img));
        opencv_core.Mat dst = new opencv_core.Mat();
        opencv_imgproc.cvtColor(src, dst, opencv_imgproc.CV_GRAY2BGR);
        opencv_core.MatVector contours = new opencv_core.MatVector();
        findContours(src, contours, opencv_imgproc.RETR_LIST, opencv_imgproc.CHAIN_APPROX_NONE);
        for (long i = 0; i < contours.size(); ++i) {
            if (contours.get(i).size(0) < 6) {
                continue;
            }
            ellipse(dst, fitEllipse(contours.get(i)), opencv_core.Scalar.CYAN, 3, opencv_core.LINE_8);
        }
        return new Java2DFrameConverter().convert(toMat.convert(dst));
    }

    public static Function<BufferedImage, BufferedImage> scale(int width, int height) {
        return (BufferedImage src) -> {
            BufferedImage dest = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            double scale = Math.min((double) src.getWidth() / width, (double) src.getHeight() / height);
            for (int y = 0; y < height; y++) {
                for (int x = 0; x < width; x++) {
                    dest.setRGB(x, y, src.getRGB((int) (x * scale), (int) (y * scale)));
                }
            }
            return dest;
        };
    }
    
    public static BufferedImage bayer(BufferedImage src) {
        BufferedImage dest = new BufferedImage(src.getWidth(), src.getHeight(), BufferedImage.TYPE_INT_RGB);
        for (int y = 0; y < src.getHeight(); y++) {
            for (int x = 0; x < src.getWidth(); x++) {
                dest.setRGB(x, y, Bayer.at(x, y).filter(src.getRGB(x, y)));
            }
        }
        return dest;
    }

    public static Function<BufferedImage, BufferedImage> shrink(int scale) {
        return (BufferedImage src) -> {
            BufferedImage dest = new BufferedImage(src.getWidth(), src.getHeight(), BufferedImage.TYPE_INT_RGB);
            for (int y = 0; y < src.getHeight(); y++) {
                for (int x = 0; x < src.getWidth(); x++) {
                    final int gray = gray(src.getRGB(x - (x%scale), y - (y%scale)));
                    int color = gray > 0xAA ? 0xFFFFFF : gray > 0x44 ? 0x888888 : 0;
                    dest.setRGB(x, y, color);
                }
            }
            return dest;
        };
    }

    public static int gray(int rgb) {
        return  Math.max(rgb & 0xFF0000 >> 16, Math.max(rgb & 0xFF00 >> 8, rgb & 0xFF));
    }
}
